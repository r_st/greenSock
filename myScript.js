// background video animation style

const tl = new TimelineLite(
//     {onComplete:function() {
//   this.restart();
// }}
);


// (function (){

  // Play video
  function animateSlide(slideNumber, startTime) {
    let slide = document.getElementById(slideNumber);
    let video = document.getElementById(slideNumber + 'Video');
    let link = document.getElementById(slideNumber + 'Link');
    let linkText = document.getElementById(slideNumber + 'LinkText');

    video.currentTime = startTime;
    tl.set(slide, {className: "-=noneDisplay"});
    tl.add(function () {video.pause()});
    tl.set(slide, {top: 0}).from(slide, 1, {top: 800});
    tl.to(video, 1, {className: "+=myVideo-fullScreen"});
    tl.add(function() {video.play()});
    // animation navigation menu in
    tl.from(linkText, 0.5, {left: 500}).set(link, {className: "+=activeNavigationLi"});
  }



//  minimize video
function animateSlideOut(newSlideID) {
  let activeSlide = document.getElementsByClassName("myVideo-fullScreen");
  let activeSlideId = document.getElementById(activeSlide[0].id);
  let activeSlideNumber = activeSlide[0].id.substring(0, activeSlide[0].id.length -5);
  let link = document.getElementById(activeSlideNumber + 'Link');
  let linkText = document.getElementById(activeSlideNumber + 'LinkText');
  let video = document.getElementById(activeSlide[0].id);

  tl.to(video, 1, {className: "-=myVideo-fullScreen"});
  tl.add(function () {video.pause()});
  // animation navigation menu out
  tl.to(linkText, 1, {top: -50, opacity: 0}).set(link, {className: "-=activeNavigationLi"});
  tl.to(activeSlideId, 1, {top: -800});
  tl.set(activeSlideId, {className: "+=noneDisplay"});

  // play new video
  // document.getElementById('slideOneLink').onclick = takeID;
  // document.getElementById('slideTwoLink').onclick = takeID;
  // document.getElementById('slideThreeLink').onclick = takeID;
  // function takeID() {
  //   let newSlideID = this.id;
  //   console.log(newSlideID);
  // }

  $("#navigation").click(function(event) {
    var text = event.target.id;
    // console.log(text);
    if (text='slideTwoLink') {
      var textLink = text.substring(0, text.length -4);
      console.log(textLink)
    }
  });


}

  animateSlide('slideOne', 0);
  // animateSlide('slideTwo', 0);
  // animateSlide('slideThree', 30);














// animation for slide one
//   tl.set(slideOne, {top: 0}).from(slideOne, 1, {top: 800});
//   tl.to(slideOneVideo, 1, {className: "+=myVideo-fullScreen"});
//   // animation navigation menu in
//   tl.from(textLink01, 0.5, {left: 500}).set(link01, {className: "+=activeNavigationLi"});
//
//   tl.set(slideOneVideo, {delay: 11});
//   tl.to(slideOneVideo, 1, {className: "-=myVideo-fullScreen"});
//   // animation navigation menu out
//   tl.to(textLink01, 0.5, {top: -50, opacity: 0}).set(link01, {className: "-=activeNavigationLi"});
//
//   tl.to(slideOne, 1, {top: -800});
//   tl.set(slideOne, {className: "+=noneDisplay"});
//
// //  animation for slide two
//   tl.set(slideTwo, {className: "-=noneDisplay"});
//   tl.set(slideTwo, {top: 0}).from(slideTwo, 1, {top: 800});
//   tl.to(slideTwoVideo, 1, {className: "+=myVideo-fullScreen"});
//   // animation navigation menu in
//   tl.from(textLink02, 0.5, {left: 500}).set(link02, {className: "+=activeNavigationLi"});
//
//   tl.set(slideTwoVideo, {delay: 9.8});
//   tl.to(slideTwoVideo, 1, {className: "-=myVideo-fullScreen"});
//   // animation navigation menu out
//   tl.to(textLink02, 1, {top: -50, opacity: 0}).set(link02, {className: "-=activeNavigationLi"});
//
//   tl.to(slideTwo, 1, {top: -800});
//   tl.set(slideTwo, {className: "+=noneDisplay"});
//
// //  animation for slide three
//   tl.set(slideThree, {className: "-=noneDisplay"});
//   tl.set(slideThree, {top: 0}).from(slideThree, 1, {top: 800});
//   tl.to(slideThreeVideo, 1, {className: "+=myVideo-fullScreen"});
//   // animation navigation menu in
//   tl.from(textLink03, 0.5, {left: 500}).set(link03, {className: "+=activeNavigationLi"});
//
//   tl.set(slideThreeVideo, {delay: 14});
//   tl.to(slideThreeVideo, 1, {className: "-=myVideo-fullScreen"});
//   // animation navigation menu out
//   tl.to(textLink03, 1, {top: -50, opacity: 0}).set(link03, {className: "-=activeNavigationLi"});
//
//   tl.to(slideThree, 1, {top: -800});
//   tl.set(slideThree, {className: "+=noneDisplay"});

// })(jQuery);

